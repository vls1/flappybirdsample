﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flappy
{
    public class PipeMovement : MonoBehaviour
    {
        [SerializeField] private float pipeSpeed = 0.07f;

        void Update()
        {
            if (BirdMovement.state == BirdMovement.BirdStates.Plying)
            {
                transform.Translate(-pipeSpeed * Time.deltaTime, 0, 0);
            }
        }
    }
}
