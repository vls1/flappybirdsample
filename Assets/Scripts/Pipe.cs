﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flappy {
    public class Pipe : MonoBehaviour
    {
        [SerializeField] private int damage = 1;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Bird.instance.TakeDamage(damage);
                BirdView.PlayBirdOnCrashAnimation("BirdOnCrash");
                transform.GetChild(0).GetComponent<Renderer>().enabled = false;
                transform.GetChild(1).GetComponent<Renderer>().enabled = false;
            }
        }
    }
}
