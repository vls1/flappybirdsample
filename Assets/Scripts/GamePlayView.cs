﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Flappy.UI
{
    public class GamePlayView : MonoBehaviour
    {
        [SerializeField] private Text healthText;

        private void Update()
        {
            UpdateText(Bird.instance.BirdHealth.ToString(), healthText);
        }

        private void UpdateText(string msg, Text text)
        {
            text.text = msg;
        }
    }
}
