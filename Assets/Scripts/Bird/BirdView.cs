﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flappy
{
    [RequireComponent(typeof(Animator))]
    public class BirdView : MonoBehaviour
    {
        private static Animator birdAnimator;

        private void Start()
        {
            birdAnimator = GetComponent<Animator>();
        }

        public static void PlayBirdOnCrashAnimation(string animationName)
        {
            birdAnimator.Play(animationName);
        }
    }
}
