﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flappy
{
    [RequireComponent(typeof(CapsuleCollider2D))]
    public class Bird : MonoBehaviour
    {
        [SerializeField] private int birdHealth = 3;
        public int BirdHealth => birdHealth;

        public static Bird instance = null;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this.gameObject);
            }

            DontDestroyOnLoad(this.gameObject);
        }

        public void TakeDamage(int damage)
        {
            if (birdHealth >= damage)
            {
                birdHealth -= damage;
            }
        }
    }
}
