﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Flappy
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BirdMovement : MonoBehaviour
    {
        [SerializeField] private float jumpForce = 6f;
        [SerializeField] private float rotateAndle = 10f;

        private Rigidbody2D birdRigitbody;
        public static BirdStates state;

        public enum BirdStates
        {
            WaitingForPlayerInput,
            Plying,
            Dead
        }

        private void Awake()
        {
            birdRigitbody = GetComponent<Rigidbody2D>();
            birdRigitbody.bodyType = RigidbodyType2D.Static;
        }

        private void Update()
        {
            switch (state)
            {
                case BirdStates.WaitingForPlayerInput:
                    if (jumpInput())
                    {
                        birdRigitbody.bodyType = RigidbodyType2D.Dynamic;
                        state = BirdStates.Plying;
                        Jump();
                    }
                    break;
                case BirdStates.Plying:

                    transform.eulerAngles = new Vector3(0, 0, birdRigitbody.velocity.y * 5);

                    if (jumpInput())
                    {
                        Jump();
                    }
                    break;
                case BirdStates.Dead:
                    break;
            }
        }

        private bool jumpInput()
        {
            return Input.GetMouseButtonDown(0);
        }

        private void Jump()
        {
            birdRigitbody.velocity = Vector2.up * jumpForce;
        }
    }
}
